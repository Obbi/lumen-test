<?php

use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->post("/", ["name" => "test"])->assertResponseOk();
    }

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
    }
}
