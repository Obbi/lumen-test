#!/bin/sh
set -eux
exec docker-compose run --rm --workdir /var/www/html php-fpm vendor/bin/phpunit
