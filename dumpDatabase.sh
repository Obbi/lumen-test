#!/bin/sh
set -eux
exec docker-compose exec mariadb mysqldump -u homestead --database homestead -psecret
