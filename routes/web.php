<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
use App\User;
use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/', function (Request $request) {
	$name = $request->input("name", "fallback");
	return app('db')->transaction(function () use ($name) {
		User::create([
			"name" => $name
		]);
		return response()->json(["status" => "ok"]);
	});
});
